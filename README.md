# DECOUVERTE GIT

## Les Titres

# Titre de niveau 1
## Titre de niveau 2
### etc...



```md
# Titre de niveau 1
## Titre de niveau 2
### etc...


###### Titre de niveau 6
```

```py
print(poulet)
int(input("Donnez moi un chiffre :"))
```
## Les Commandes
Avec les magic quotes (alt-gr + è)

Lancer un programme en python : `py
script.py`


```md
Lancer un programme en python : `py
script.py`
```

## Les Notes

> Ceci est une note, elle attire l'attention sur qqchose à prendre en compte

```md
> Ceci est une note, elle attire l'attention sur qqchose à prendre en compte
```
### Les Liens ou Ancres

Permet la navigation interne OU externe

Voir [le site de Python.](https://www.python.org/)

```md
Voir [le site de Python.](https://www.python.org/)
```

##### Preferer chemins relatifs
Lien vers [le support de Git](./p1_git_base.pdf)
```md
Lien vers [le support de Git](./p1_git_base.pdf)
```

## Les images

![Image_ville](https://images.unsplash.com/photo-1624286599061-8d439e0aa080?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2134&q=80)

```md
![Image_ville](https://images.unsplash.com/photo-1624286599061-8d439e0aa080?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2134&q=80)
```

## Les Tableaux

| id | Nom | Prénom |
|----|-----|--------|
|1|Dupond|Jean|
|2|Dupond|Christophe|

```md
| id | Nom | Prénom |
|----|-----|--------|
|1|Dupond|Jean|
|2|Dupond|Christophe|
```
## Les éléments de texte
### Le gras ou emphase

Pour mettre des éléments en **valeur**
```md
Pour mettre des éléments en **valeur**
```
### L'italique

Pour attirer l'attention sur un *paramètre*
```md
Pour attirer l'attention sur un *paramètre*
```

## Les listes

### Liste non ordonnées
Pratique pour l'énumération

- Se lever
        - éteindre son réveil
        - Préparer le café
- Café
- Se brosser les dents
  
```md
- Se lever
        - éteindre son réveil
        - Préparer le café
- Café
- Se brosser les dents
```

### Liste ordonnéees
Pratique pour ordonner des éléments
Il est possible de faire des sous-listes, il faut faire deux tabulation et repartir à 1 dans la numérotation

1) France
2) Belgique
3) Pays-Bas
4) Allemagne
    1) Bayern Munchen
    1) Dortmund
    1) Hoffenheim
   
```md
1) France
2) Belgique
3) Pays-Bas
4) Allemagne
    1) Bayern Munchen
    1) Dortmund
    1) Hoffenheim
```

EDIT : COMMIT ENVOYE DIRECTEMENT VIA GITLAB
EDIT LOCAL : COMMIT ENVOYE DEPUIS LE LOCAL
EDIT2 : COMMIT ENVOYE DIRECTEMENT VIA GITLAB2
EDIT3 : COMMIT ENVOYE DIRECTEMENT VIA GITLAB3

















