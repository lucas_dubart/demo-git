# Les branches

Voir [la cheat sheet Git Flow](http://danielkummer.github.io/git-flow-cheatsheet/)

Les branches permettent de travailler sur une partie du projet sans impacter le travail des autres branches.

On peut voir les branches comme des lignes temporelles parallèles.

## Voir les branches

`git branch` affiche la liste des branches et celle sur laquelle nous nous trouvons.

```bash
$ git branch
  feature/branch
* main
```

## Créer une branche

Créer une branche **locale**
`git branch nouvelle/branche`

Il faut la pousser pour l'avoir en **remote** :
`git push --all`

Astuce : pour créer une branche et se placer directement dessus :
`git checkout -b nouvelle/branche`

## Se déplacer sur une branche

`git checkout nouvelle/branche`
(ca permet aussi de se déplacer sur des commits, "détacher sa tête")


## Renommer une branche

`git branch -m nouveau_nom`

## Supprimer une branche

`git branch -d branche_a_supprimer`

## MERGE : Fusion de branches

Pour fusionner les branches, il faut se placer sur la branche qui reçoit les modifications.
(**ATTENTION**, la branche main est la branche où le projet fonctionne, on merge donc **rarement** sur celle la!
**Branche DEVELOPP**, branche la plus à jour, où projet est mis à jour et testé)

`git checkout branche_a_mettre_a_jour`

Ensuite merger avec la branche qui contient les fonctionnalités supplémentaires
`git merge branche_avec_fonction` 


En option : Supprimer la branche.
**ATTENTION** pas la supprimer tout de suite

`git branch -d branche_a_supprimer`












